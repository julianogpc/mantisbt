#!groovy

node('julianograciano.eti.br') {
    def docker
    def curDir = pwd()

    try{
        stage('Preparation') {
            git credentialsId: 'b4bdbbfe-8f49-4fc1-ba12-0669e0d32f25', url: 'https://gitlab.com/julianogpc/mantisbt.git'
            docker = tool 'docker-17.03.2-ce'
        }

        stage('Database Configuration'){
            withCredentials([file(credentialsId: 'MANTISBT_EXTERNAL_VARS', variable: 'external_vars')]) {
                sh "rm -f external_vars.yml"
                sh "cp $external_vars external_vars.yml"
                ansiblePlaybook colorized: true, disableHostKeyChecking: true, installation: 'ansible-2.7.5', playbook: 'do_deploy_mantisbt_config_db.yml'
            }
        }

        stage('Deploy MantisBT') {
            withCredentials([file(credentialsId: 'MANTISBT_CONFIG', variable: 'config_inc_mod')]) {
                withEnv(['DOCKER_CONTENT_TRUST=0']) {
                    withDockerRegistry(credentialsId: 'b4bdbbfe-8f49-4fc1-ba12-0669e0d32f25', toolName: 'docker-17.03.2-ce', url: 'https://registry.gitlab.com') {
                        sh "rm -f config_inc_mod.php"
                        sh "cp $config_inc_mod config_inc_mod.php"
                        sh "chmod 444 config_inc_mod.php external_vars.yml"
                        sh "docker run -u root --rm -v ${curDir}:/usr/src/ansible/mantisbt --volumes-from letsencrypt registry.gitlab.com/julianogpc/ansible ansible-playbook mantisbt/do_deploy_mantisbt_deploy.yml"
                    }
                }
            }
        }

        stage('Configuring Access to MantisBT'){
            ansiblePlaybook colorized: true, disableHostKeyChecking: true, installation: 'ansible-2.7.5', playbook: 'do_deploy_mantisbt_config_access.yml'
        }

        stage('Remove "admin" Directory and Config e-mail') {
            withEnv(['DOCKER_CONTENT_TRUST=0']) {
                withDockerRegistry(credentialsId: 'b4bdbbfe-8f49-4fc1-ba12-0669e0d32f25', toolName: 'docker-17.03.2-ce', url: 'https://registry.gitlab.com') {
                    sh "docker run -u root --rm -v ${curDir}:/usr/src/ansible/mantisbt --volumes-from letsencrypt registry.gitlab.com/julianogpc/ansible ansible-playbook mantisbt/do_deploy_mantisbt_config_email.yml"
                }
            }
        }

        stage('Restore Database'){
            withCredentials([file(credentialsId: 'MANTISBT_DUMP_FILE', variable: 'dump_file')]) {
                sh "rm -f *.sql"
                sh "cp $dump_file mantisbt.sql"
                ansiblePlaybook colorized: true, disableHostKeyChecking: true, installation: 'ansible-2.7.5a', playbook: 'do_deploy_mantisbt_config_db_restore.yml'
            }
        }
    }catch(any){
        throw any
    }finally{
        deleteDir()
    }
}