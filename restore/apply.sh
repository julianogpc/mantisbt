kubectl create secret generic mantisbt \
--from-file=dump=./mantisbt.sql \
--from-file=config=./config_inc_mod.php \
--from-file=config_access=./ansible_mantisbt_config_access.yml \
--from-file=config_db=./ansible_mantisbt_config_db.yml \
--from-file=external_vars=./external_vars.yml
kubectl -f mantisbt-restore-db-job.yml replace --force 
POD_NAME=$(kubectl get po -l 'job-name in (mantisbt-restore-db)' -o name)
kubectl wait --for=condition=Ready $POD_NAME --timeout=600s
kubectl attach -i $POD_NAME